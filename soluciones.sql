﻿/**
  Ejercicio 1
  Crear un disparador para la tabla ventas para que cuando metas un registro 
  nuevo te calcule el total automáticamente
**/

DELIMITER //
CREATE OR REPLACE TRIGGER ventasBI
	BEFORE INSERT
	ON ventas
	FOR EACH ROW
BEGIN
  -- cuando metas un registro 
  -- nuevo te calcule el total automáticamente
    SET NEW.total=new.unidades*new.precio;
END //

DELIMITER ;

/**
  Ejercicio 2
  Crear un disparador para la tabla ventas para que cuando inserte un registro 
  me sume el total a la tabla productos (en el campo cantidad).
**/

DELIMITER //
CREATE OR REPLACE TRIGGER ventasAI
	AFTER INSERT
	ON ventas
	FOR EACH ROW
BEGIN
  /*
    Crear un disparador para la tabla ventas para que cuando inserte un registro 
    me sume el total a la tabla productos (en el campo cantidad).
  */
    UPDATE productos p SET p.cantidad=p.cantidad+new.total 
      WHERE p.producto=new.producto;
END //
DELIMITER ;

/**
  Ejercicio 3
  Crear un disparador para la tabla ventas para que cuando actualices un registro 
  nuevo te calcule el total automáticamente.
**/

DELIMITER //
CREATE OR REPLACE TRIGGER ventasBU
	BEFORE UPDATE
	ON ventas
	FOR EACH ROW
BEGIN
  /**
  Crear un disparador para la tabla ventas para que cuando actualices un registro 
  nuevo te calcule el total automáticamente.
  **/
    SET NEW.total=new.unidades*new.precio;
  -- esto es preferible realizarlo en el after
    UPDATE productos 
      set cantidad=cantidad+(new.total-old.total)
      WHERE producto=new.producto;
END //
DELIMITER ;

/**
  Ejercicio 4
  Crear un disparador para la tabla ventas para que cuando actualice un registro 
  me sume el total a la tabla productos (en el campo cantidad).
**/

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventasAU
  	AFTER UPDATE
  	ON ventas
  	FOR EACH ROW
  BEGIN
    /**
    Crear un disparador para la tabla ventas para que cuando actualice un registro 
    me sume el total a la tabla productos (en el campo cantidad).
    **/
      UPDATE productos p 
        SET p.cantidad=p.cantidad+(new.total-old.total) 
        WHERE p.producto=new.producto;
  END //
  DELIMITER ;

/**
  Ejercicio 5
  Crear un disparador para la tabla productos que si cambia el código del producto 
  te sume todos los totales de ese producto de la tabla ventas
**/
DELIMITER //
CREATE OR REPLACE TRIGGER productosBU
	BEFORE UPDATE
	ON productos
	FOR EACH ROW
BEGIN
  /**
  Crear un disparador para la tabla productos que si cambia el código del producto 
  te sume todos los totales de ese producto de la tabla ventas
  **/
    SET NEW.cantidad=(
          SELECT sum(v.total) FROM ventas v 
            WHERE v.producto=NEW.producto
          );
END //
DELIMITER ;

/**
  Ejercicio 6
  Crear un disparador para la tabla productos que si eliminas 
  un producto te elimine todos los productos del mismo código 
  en la tabla ventas

**/
DELIMITER //
CREATE OR REPLACE TRIGGER productosAD
	AFTER DELETE
	ON productos
	FOR EACH ROW
BEGIN
/**
  Crear un disparador para la tabla productos que si eliminas 
  un producto te elimine todos los productos del mismo código 
  en la tabla ventas
**/
    DELETE FROM ventas 
      WHERE producto=old.producto;
END //
DELIMITER ;

/**
  Ejercicio 7
  Crear un disparador para la tabla ventas que si eliminas un 
  registro te reste el total del campo cantidad de la tabla 
  productos (en el campo cantidad).
**/

DELIMITER //
CREATE OR REPLACE TRIGGER ventasAD
	AFTER DELETE
	ON ventas
	FOR EACH ROW
BEGIN
/**
  Crear un disparador para la tabla ventas que si eliminas un 
  registro te reste el total del campo cantidad de la tabla 
  productos (en el campo cantidad).
**/
    UPDATE productos 
      SET cantidad=cantidad-old.total 
      WHERE producto=old.producto;
END //

DELIMITER ;

